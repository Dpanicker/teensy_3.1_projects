/*_6915995_david_panicker_lab_7_task3
 serial rx2
 This code waits for a $ signifying the start of transmission
 places the bytes that follow into an array called recBuffer[]
 until a CR is received. Be sure to select carriage return from 
 the pull-down menu on the comm windows so that a CR is sent when
 you click on the send button.
 char GPSstring[] = "$GPGGA,014729.10,4303.5753,N,08019.0810,W,1,6,1.761,214.682,M,0,M,0,*5D";
 //                Latitude 4     5      6 7 8 9        10 11 12 13 14 15
 
 */

static char recBuffer [255];  // gloabal value

int msgrec = 0;

void setup()
{
  Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps
}
int GPSstringReceive(); //protoype declaration
int checkForGPSstring (); //protoype declaration

void loop()
{
  checkForRecvdChar();
  if(msgrec == 1){
    GPSstringReceive();
    msgrec = 0;
  }
}
int checkForRecvdChar()
{
  static int recByteCtr = 0; //declare and initialize the rec'd byte counter variable
  char byteRead;
  while (Serial.available()) //then let's read the byte
  { 
    if (recByteCtr == 0) //then check to see if it's the start of a message
    {
      byteRead = Serial.read(); //read the byte
      if (byteRead == '$')
      {
        recBuffer[recByteCtr] = byteRead; //copy the byte read into the rec buffer
        recByteCtr++;
      }
    }
    if (recByteCtr != 0) //then we're reading bytes after receiving an STX in the message stream
    {
      byteRead = Serial.read(); //read the byte
      if (byteRead != 13) //13 = CR
      {
        recBuffer[recByteCtr] = byteRead; //copy the byte read into the rec buffer
        recByteCtr++;
      }
      else //recbyte == 13
      {
        recBuffer[recByteCtr] = '\0'; //null terminate the rec'd string in rec buffer
        recByteCtr = 0;
        Serial.println(recBuffer);
        msgrec = 1;
      }
    }
  }

}
int GPSstringReceive()
{    
  const char *delim  = ",";   //a comma is the delimiter
  const char *delim1 = ".";
  char *firstItem;
  char *secondItem;
  char *thirdItem;
  char *fourthItem;
  char *fifthItem;
  char *sixthItem;
  char *seventhItem;
  char *eighthItem;
  char *ninethItem;
  char *tenthItem;
  char *eleventhItem;
  char *twelvethItem;
  char *thirteenthItem;
  char *fourteenthItem;
  char *fifteenthItem;
  char *firstItem2;
  char *secondItem2;
  char *thirdItem2;
  //  char *fourthItem2;
  char *secDecimal;
  char *latitudeDecimal;
  char *longitudeDecimal;
  firstItem = strtok(recBuffer,delim); 
  secondItem = strtok(NULL,delim);
  thirdItem = strtok(NULL,delim);
  fourthItem = strtok(NULL,delim);
  fifthItem = strtok(NULL,delim);
  sixthItem = strtok(NULL,delim);
  seventhItem = strtok(NULL,delim);
  eighthItem =strtok(NULL,delim);
  ninethItem =strtok(NULL,delim);
  tenthItem =strtok(NULL,delim);
  eleventhItem =strtok(NULL,delim);
  twelvethItem =strtok(NULL,delim);
  thirteenthItem =strtok(NULL,delim);
  fourteenthItem =strtok(NULL,delim);
  fifteenthItem =strtok(NULL,delim);
  firstItem2 = strtok(secondItem, delim1);   // get 014729 
  secDecimal = strtok(NULL, delim1);   //  get   .10  decimal
  secondItem2 = strtok(thirdItem, delim1); // get 4303
  latitudeDecimal = strtok(NULL, delim1);  // get 5753
  thirdItem2 = strtok(fifthItem, delim1); // get 08019
  longitudeDecimal = strtok(NULL, delim1); // get 0810
  int UTCtime = 0;
  int decimal = 0; 
  char buffer[255];
  int latitude = 0;
  int latDecimal = 0;
  //char buffer1[255]; // latitude
  int longitude = 0;
  int longDecimal = 0;
  //char buffer2[255]; // longitude
  long Check;
  //char buffer3[255];
  Serial.print ("Sentence ID: ");
  Serial.println(firstItem);
  UTCtime = atoi(secondItem);    // UTCtime to int
  decimal = atoi(secDecimal); // 
  sprintf(buffer,"UTC time: %d hr %d min %d.%d sec", UTCtime / 10000, UTCtime%10000/100, UTCtime%10000%100,decimal);
  Serial.println(buffer);
  // sprint latitude 43degrees 3 minutes 5753 decimal degrees
  latitude = atoi(thirdItem);   //  get integer 4303
  latDecimal = atoi(latitudeDecimal);
  sprintf(buffer, "Latitude: %d degrees %d minute %d decimal degrees", latitude / 100, latitude % 100,latDecimal);
  Serial.print(buffer);
  Serial.println(fourthItem);
  //80 degrees 19 minutes 0810 decimal degrees
  longitude = atoi(fifthItem);   //  get integer 4303
  longDecimal = atoi(longitudeDecimal);
  sprintf(buffer, "Longitude: %d degrees %d minute %d decimal degrees", longitude / 100, longitude % 100,longDecimal);
  Serial.print(buffer);
  Serial.println(sixthItem);
  Serial.print ("Position Fix:");
  Serial.println(seventhItem);
  Serial.print("Satellites Used:");
  Serial.println(eighthItem);
  Serial.print("HDOP 38466 Horizontal dilution of precision:");
  Serial.println(ninethItem);
  Serial.print("Altitude:");
  Serial.print(tenthItem);
  Serial.println(eleventhItem);
  Serial.print("Geoid separation:");
  Serial.print(twelvethItem);
  Serial.println(thirteenthItem);
  Serial.print("DGPS Age:");
  Serial.println(fourteenthItem);
  Serial.print("Checksum:");
  Serial.println(fifteenthItem);
  Check=strtoul(fifteenthItem+1,NULL,16);
  sprintf(buffer,"check sum in byte: %x",Check);
  Serial.println(buffer);
}

