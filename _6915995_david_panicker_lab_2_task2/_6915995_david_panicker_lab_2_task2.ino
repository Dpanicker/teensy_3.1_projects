/* 6915995 david panicker lab 2 task2
*/

// Teensy 2.0 has the LED on pin 11
// Teensy++ 2.0 has the LED on pin 6
// Teensy 3.0 has the LED on pin 13
const int ledPin = 13;

// the setup() method runs once, when the sketch starts

void setup() {
  Serial.begin(9600);
  delay(3000);
  Serial.println("Starting lab2 task2");
  
 
}

// the loop() methor runs over and over again,
// as long as the board has power

void loop() {
 
Serial.print("The size in bytes of datatype int8_t  is: ");
Serial.println(sizeof(int8_t));
delay(1000);
Serial.print("The size in bytes of datatype int16_t is: ");
Serial.println(sizeof(int16_t));
delay(1000);
Serial.print("The size in bytes of datatype int32_t is: ");
Serial.println(sizeof(int32_t));
delay(1000);
Serial.print("The size in bytes of datatype int64_t is: ");
Serial.println(sizeof(int64_t));
delay(1000);
Serial.print("The size in bytes of datatype int     is: ");
Serial.println(sizeof(int));
delay(1000);
Serial.print("The size in bytes of datatype char    is: ");
Serial.println(sizeof(char));
delay(1000);
Serial.print("The size in bytes of datatype byte    is: ");
Serial.println(sizeof(byte));
delay(1000);
Serial.print("The size in bytes of datatype long    is: ");
Serial.println(sizeof(long));
delay(1000);
Serial.print("The size in bytes of datatype double  is: ");
Serial.println(sizeof(double));
delay(1000);
Serial.print("The size in bytes of datatype float   is: ");
Serial.println(sizeof(float));
delay(1000);
Serial.print("The size in bytes of datatype double  is: ");
Serial.println(sizeof(double));
delay(1000);

}

