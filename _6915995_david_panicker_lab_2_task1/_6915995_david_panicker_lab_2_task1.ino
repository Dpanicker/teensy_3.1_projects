/* 6915995 david panicker lab 2 task1
*/

// Teensy 2.0 has the LED on pin 11
// Teensy++ 2.0 has the LED on pin 6
// Teensy 3.0 has the LED on pin 13
const int ledPin = 13;

// the setup() method runs once, when the sketch starts

void setup() {
  Serial.begin(9600);
  delay(3000);
  Serial.println("Starting lab2 task1");
  
 
}

// the loop() methor runs over and over again,
// as long as the board has power

void loop() {
 

  Serial.print("David Panicker\n");
  delay(1000);

}

