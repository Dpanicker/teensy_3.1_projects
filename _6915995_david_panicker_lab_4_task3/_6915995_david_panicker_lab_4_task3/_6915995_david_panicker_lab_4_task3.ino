// _6915995_david_panicker_lab_4_task3

const int tonePin = 23;
int scale[]= { 392 , 440, 494, 523, 587, 659, 699, 794 };

void setup()
{  

  pinMode(tonePin, OUTPUT);     
}
void loop() 
{
  char hbday[] = "@@A@3CB@@A@DC@@GECBAFFECDC";
  playsong(hbday); 
}
int8_t playsong(char song[])
{
  int x=0;
  int notetoplay;
  while (song[x])
  {
    if(song[x]-64<0 || song[x]-64>7)             // used for subtracting the ASCII value
    {
      Serial.println("values are out of range");
      return -1;
  }
    else
    {
      notetoplay=scale[song[x]-64];
      playNote(notetoplay);
      x++;      
    }
  }
  return 1;
}
void playNote(int freq) 
{
  tone (23,freq,300);
  delay(310); 
}







