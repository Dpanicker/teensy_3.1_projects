// _6915995_david_panicker_lab_6_task3
const int led1=23;
const int led2=22;
const int led3=21;
void setup()
{
  Serial.begin(9600);
  pinMode(led1,OUTPUT);
  pinMode(led2,OUTPUT);
  pinMode(led3,OUTPUT);
}
void loop()
{
  get_and_exec_comnd();
}
void get_and_exec_comnd()
{
  int incomingVal;
  int incomingVal1;
  int16_t incomingVal2;
  if (Serial.available() > 0 )
  {
    incomingVal = Serial.parseInt();     //for the led number
    incomingVal1 = Serial.parseInt();    // on/off state of led 
    incomingVal2 = Serial.parseInt();    // led brightness
    Serial.print("For incomingVal You entered: ");
    Serial.println(incomingVal);  //prints the led number user typed in
    Serial.print("For incomingVal1 You entered: ");
    Serial.println(incomingVal1); //prints on or off state of led user typed in
    Serial.print("For incomingVal2 You entered: ");
    Serial.println(incomingVal2); //prints the led brightness user typed in
  }
  if (incomingVal==1&&incomingVal1==0)          analogWrite(led1,0);
  else  if (incomingVal==1&&incomingVal1==1)    analogWrite(led1,incomingVal2);//lights up led1 with the input brightness from the user
  else  if (incomingVal==2&&incomingVal1==0)    analogWrite(led2,0);
  else  if (incomingVal==2&&incomingVal1==1)    analogWrite(led2,incomingVal2);//lights up led2 with the input brightness from the user
  else  if (incomingVal==3&&incomingVal1==0)    analogWrite(led3,0);
  else  if (incomingVal==3&&incomingVal1==1)    analogWrite(led3,incomingVal2); //lights up led3 with the input brightness from the user 
}










