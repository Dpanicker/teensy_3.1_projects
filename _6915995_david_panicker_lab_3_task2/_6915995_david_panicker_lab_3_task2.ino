/* 6915995 david panicker lab 3 task2*/

const int ledPin = 16 ;


//prototypes


void setup() 
{
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600); 
}

void loop() 
{
  flashSOS();


}  

int8_t  flash_Led(int16_t ledon,int16_t ledoff)
{
  if((1<ledon<10000)&&(1<ledoff<10000))
  {
    digitalWrite(ledPin, HIGH);   // set the LED on
    delay(ledon);                  // wait for input as per ledon
    digitalWrite(ledPin, LOW);    // set the LED off
    delay(ledoff);                // wait for input as per ledoff

      Serial.println("1"); //to check if the "if" statement is working 
    return 1;     
  }
  else 
  {
    Serial.println("-1"); //to check if the "if" statement is working 
    return -1;           
  }

}

void flashSOS()
{


  flashDot();
  flashDot();
  flashDot();      // for the letter "S"
  flashDash(); 
  flashDash();
  flashDash();   // for the letter "O"
  flashDot();
  flashDot();
  flashDot();      // for the letter "S"
  delay(3000); 

}

void flashDot()
{
  flash_Led(200,200); 
}

void flashDash()
{
  flash_Led(500,200);
}










