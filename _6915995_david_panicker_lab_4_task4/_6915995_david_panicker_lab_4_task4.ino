// _6915995_david_panicker_lab_4_task4

const	int	g=392; // assigning const 392 to label g
const	int	A=440; // assigning const 440 to label A
const	int	B=494; // assigning const 494 to label B
const	int	C=523; // assigning const 523 to label C
const	int	D=587; // assigning const 587 to label D
const	int	E=659; // assigning const 659 to label E
const	int	F=699; // assigning const 699 to label F
const	int	G=794; // assigning const 794 to label G

const int tonePin = 23;

void setup()
{  
  Serial.begin(9600);
  pinMode(tonePin, OUTPUT);     
}
void loop() 
{
  int hbday[] = {
    g,g,A,g,C,3,B,g,g,A,g,D,C,g,g,G,E,C,B,A,F,F,E,C,D,C,};
  playsong(hbday); 
}
int8_t playsong(int song[])
{
  int x=0;
  while(song[x])
  {
    if(song[x]<392 || song[x]>794)
    {
      Serial.println("Values out of range");
      return -1;
    }
    else
    {
      playNote(song[x]);
      x++;
    }  
  }
  //if we're here all notes played
  //w/o error
  return 1;
} 
void playNote(int freq) 
{
  tone (23,freq,300);
  delay(310); 
}






