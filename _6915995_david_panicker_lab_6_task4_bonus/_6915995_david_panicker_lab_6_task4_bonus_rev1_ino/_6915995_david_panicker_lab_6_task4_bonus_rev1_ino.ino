/*_6915995_david_panicker_lab_6_task4
 serial rx2
 This code waits for a $ signifying the start of transmission
 places the bytes that follow into an array called recBuffer[]
 until a CR is received. Be sure to select carriage return from 
 the pull-down menu on the comm windows so that a CR is sent when
 you click on the send button.
 char GPSstring[] = "$GPGGA,014729.10,4303.5753,N,08019.0810,W,1,6,1.761,214.682,M,0,M,0,*5D";
 
 */

char RecBuffer [255];// gloabal value
int msgflag =0;
void setup()
{
  Serial.begin(9600);// opens serial port, sets data rate to 9600 bps
}
void loop()
{
  CheckForRecvChar();
  if (msgflag == 1)
  { 
    parse();
    msgflag=0;//reset the flag
  }
}
void CheckForRecvChar()
{
  int16_t RecBufferCtr = 0;//declare and initialize the rec'd byte counter variable
  char ByteRecv;
  while (Serial.available()) //then let's read the byte
  {
    if (RecBufferCtr == 0)//then check to see if it's the start of a message
    {
      ByteRecv=Serial.read();//read the byte
      if (ByteRecv == '$')
      { 
        RecBuffer[RecBufferCtr] = ByteRecv;//copy the byte read into the rec buffer
        RecBufferCtr++;
      }
    }
    if (RecBufferCtr != 0)//then we're reading bytes after receiving an STX in the message stream
    { 
      ByteRecv=Serial.read();//read the byte
      if (ByteRecv != 13)//13 = CR
      { 
        RecBuffer[RecBufferCtr] = ByteRecv;//copy the byte read into the rec buffer
        RecBufferCtr++;
      }
      else//recbyte == 13
      {
        RecBuffer[RecBufferCtr] = '\0';//null terminate the rec'd string in rec buffer
        RecBufferCtr= 0;
        msgflag = 1;
      }
    }
  }
}
void parse()
{
  char* delim =",";//a comma is the delimiter
  char* delim1=".";//
  char* SentenceID = NULL;//initialising pointer of char type
  char* UTC_time = NULL;
  char* Latitude = NULL;
  char* Latitudedir = NULL;
  char* Longitude = NULL;
  char* Longitudedir = NULL;
  char *firstItem2;
  char *secondItem2;
  char *thirdItem2;
  char *secDecimal;
  char *latitudeDecimal;
  char *longitudeDecimal;
  int UTCtime =0;
  int decimal =0;
  int latitude= 0;
  int latDecimal=0;
  int longitude=0;
  int longDecimal=0;
  char buffer [256];

  //Breaking the array and storing it in the vartiables as strings
  SentenceID=strtok(RecBuffer,delim);
  UTC_time=strtok(NULL,delim);
  Latitude=strtok(NULL,delim);
  Latitudedir=strtok(NULL,delim);
  Longitude=strtok(NULL,delim);
  Longitudedir=strtok(NULL,delim);
  firstItem2 = strtok(SentenceID, delim1);   // get 014729 
  secDecimal = strtok(NULL, delim1);   //  get   .10  decimal
  secondItem2 = strtok(Latitude, delim1); // get 4303
  latitudeDecimal = strtok(NULL, delim1);  // get 5753
  thirdItem2 = strtok(Longitude, delim1); // get 08019
  longitudeDecimal = strtok(NULL, delim1); // get 0810

  //printing the values stores by the variables
  latitude = atoi(Latitude);   //  get integer 4303
  latDecimal = atoi(latitudeDecimal);
  sprintf(buffer, "Latitude: %d degrees %d minute %d decimal degrees", latitude / 100, latitude % 100,latDecimal);
  Serial.print(buffer);
  Serial.println(Latitudedir);
  longitude = atoi(Longitude);   //  get integer 4303
  longDecimal = atoi(longitudeDecimal);
  sprintf(buffer, "Longitude: %d degrees %d minute %d decimal degrees", longitude / 100, longitude % 100,longDecimal);
  Serial.print(buffer);
  Serial.println(Longitudedir);  
}








