/* 6915995 david panicker lab 6 task4
 */
char RecBuffer [255];
int msgflag =0;
void setup()
{
   Serial.begin(9600);
}
void loop()
{
  CheckForRecvChar();
  if (msgflag == 1)
  { 
    parse();
    msgflag=0;
  }
}
void CheckForRecvChar()
{
  int16_t RecBufferCtr = 0;
  char ByteRecv;
  while (Serial.available()) 
  {
    if (RecBufferCtr == 0)
    {
      ByteRecv=Serial.read();
      if (ByteRecv == '$')
      { 
        RecBuffer[RecBufferCtr] = ByteRecv;
        RecBufferCtr++;
      }
    }
    if (RecBufferCtr != 0)
    { 
      ByteRecv=Serial.read();
      if (ByteRecv != 13)
      { 
        RecBuffer[RecBufferCtr] = ByteRecv;
        RecBufferCtr++;
      }
      else
      {
        RecBuffer[RecBufferCtr] = '\0';
        RecBufferCtr= 0;
        Serial.println(RecBuffer);
        msgflag = 1;
      }
    }
  }
}
void parse()
{
  char* delim =",";
  char* SentenceID = NULL;
  char* UTC_time = NULL;
  char* Latitude = NULL;
  char* Latitudedir = NULL;
  
  SentenceID=strtok(RecBuffer,delim);
  UTC_time=strtok(NULL,delim);
  Latitude=strtok(NULL,delim);
  Latitudedir=strtok(NULL,delim);

  Serial.println(SentenceID);
  Serial.println(UTC_time);
  Serial.println(Latitude);
  Serial.println(Latitudedir);
}







