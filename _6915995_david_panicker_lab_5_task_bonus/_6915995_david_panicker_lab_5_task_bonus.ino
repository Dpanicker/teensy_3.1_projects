
// _6915995_david_panicker_lab_5_task_bonus
const float top_value_to_display = 30.0;
const float the_bottom_value_to_display = -5.0;
const float range_of_value_to_display = top_value_to_display - the_bottom_value_to_display ; 
const int16_t precision = 100; 
 
int8_t my_pot = 14; // my potentiometer, which is the source of the analog voltage will be connected to pin 14
float the_resolution_factor = range_of_value_to_display/1023.0; 
char strBuffer [256]; 

//Defining the degree constant
unsigned char degree = 186; // I found it using iterating through all the values using a for looop

void setup() {                
   pinMode(my_pot, INPUT); 
   Serial.begin (9600);//for debugging
  delay ( 1000 ); // debug
}
void loop() 
{
  int16_t analog_voltage_equ_num = 0;
  float voltage_value = 0.0;
  analog_voltage_equ_num = analogRead(my_pot);
  voltage_value =  (float)analog_voltage_equ_num * the_resolution_factor; 
  float voltage_prec_multiplied_value = voltage_value * (float) precision; // Taking the voltage value and multiplied with precision, then converted to integer
  float voltage_prec_main_num = (float) (voltage_prec_multiplied_value)/ (float)(precision);// This is the main part of the number. i.e if the number is 321; it will return 32;
  // For scaling it to -5 value
  float the_value_of_temp = voltage_prec_main_num - 5.0; // -0 is zero so this won't print the correct result. Therefore 
  uint8_t voltage_prec_decimal_num = (int16_t)voltage_prec_multiplied_value % precision; 
  if ( (the_value_of_temp < 0) && the_value_of_temp >-1)
  {
    sprintf(strBuffer," The temparature is  : -%d.%02d %c C", (int8_t) the_value_of_temp, voltage_prec_decimal_num,degree);
  }
  else
  {
     sprintf(strBuffer," The temparature is  : %d.%02d%c C", (int8_t) the_value_of_temp, voltage_prec_decimal_num,degree); 
  Serial.println(strBuffer); 
  delay(500);
}
}




