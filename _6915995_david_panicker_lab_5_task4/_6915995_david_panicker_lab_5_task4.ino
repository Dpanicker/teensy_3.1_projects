
// _6915995_david_panicker_lab_5_task4
//pin 23 choosen as Analog pin

const int Pot = 23;
char strBuffer[256];//buffer where the resulting C-string is stored
float trimpot_100 ;
const float x= 0.097751711;//scaling factor



// the setup() method runs once, when the sketch starts

void setup() {
  pinMode(Pot,INPUT);
  Serial.begin(9600);

}
void loop() {
  int16_t val;
  val=analogRead(Pot);
  trimpot_100=x*(float)val;//typecast
  sprintf(strBuffer,"pot is set to : %d",(int)trimpot_100);
  Serial.println(strBuffer);
  delay(500);
  
}





