/* 6915995 david panicker lab 3 task6*/

const int redPin =  23; // pin 23 choosen to display the pwm function through led brightness


void setup()   
{                
  pinMode(redPin, OUTPUT);

}

void loop()                     
{
  HeartBeat(); //calling the heart beat function 
  delay(250);  //delay of 250ms
  HeartBeat1();
  delay(500);  //delay of 500ms
  delay(500);  //delay of 500ms



}

void HeartBeat()
{
  int16_t i;
  for(i=0;i<=255;i++)
  {
    analogWrite(redPin,i); //led brightness from 0 – full brightness for a short time period
    delay(1);
  }

}

void HeartBeat1()
{
  int16_t i;
  for(i=0;i<=255;i++)
  {
    analogWrite(redPin,i); //led brightness from 0 – full brightness for a short time period
    delay(2);
  }

} 

