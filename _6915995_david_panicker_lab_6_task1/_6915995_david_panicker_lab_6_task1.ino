// _6915995_david_panicker_lab_6_task1
const int led1=23;
const int led2=15;
const int led3=14;
void setup()
{
  Serial.begin(9600);
  pinMode(led1,OUTPUT);
  pinMode(led2,OUTPUT);
  pinMode(led3,OUTPUT);
}

void loop()
{
  get_and_exec_comnd();
}
void get_and_exec_comnd()
{
  int incomingVal;
  int incomingVal1; 
  if (Serial.available() > 0 ) //then chars are in the serial buffer
  {
    incomingVal = Serial.parseInt(); //for the led number
    incomingVal1 = Serial.parseInt(); // on & off state of led
    Serial.print("You entered: ");  
    Serial.println(incomingVal); //prints the led number user typed in
    Serial.print("You entered val : ");
    Serial.println(incomingVal1);//prints on or off state of led user typed in
  }
  if (incomingVal==1&&incomingVal1==0)          digitalWrite(led1,LOW); //turns the led off
  else  if (incomingVal==1&&incomingVal1==1)    digitalWrite(led1,HIGH);//turns the led on
  else  if (incomingVal==2&&incomingVal1==0)    digitalWrite(led2,LOW); //turns the led off
  else  if (incomingVal==2&&incomingVal1==1)    digitalWrite(led2,HIGH);//turns the led on
  else  if (incomingVal==3&&incomingVal1==0)    digitalWrite(led3,LOW); //turns the led off
  else  if (incomingVal==3&&incomingVal1==1)    digitalWrite(led3,HIGH);//turns the led on
}



