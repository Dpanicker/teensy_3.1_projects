/*
_2014 Final Exam Task 2.ino
This code waits for a * signifying the start of transmission
places the bytes that follow into a Global array called recBuffer[]
until a CR is received. Be sure to select carriage return from 
the pull-down menu on the comm windows so that a CR is sent when
you click on the send button.
*922,5.12,12.01,-11.58
*/

static int recByteCtr = 0; //declare and initialize the rec'd byte counter variable
int8_t checkForRecvdChar (); //protoype declaration
static char recBuffer [255];

void setup()
{
   Serial.begin(9600);     // opens serial port, sets data rate to 9600 bps
   Serial.println("starting");
   pinMode(6,OUTPUT);
}

void loop()
{
  checkForRecvdChar();
}

int8_t checkForRecvdChar()
{
  static char recBuffer [255];
  char byteRead;
 
  while (Serial.available()) //then let's read the byte
  { 
    if (recByteCtr == 0) //then check to see if it's the start of a message
    {
      byteRead = Serial.read(); //read the byte
      if (byteRead == '*')
      {
        recBuffer[recByteCtr] = byteRead; //copy the byte read into the rec buffer
        recByteCtr++;
      }
    }
    if (recByteCtr != 0) //then we're reading bytes after receiving an STX in the message stream
    {
      byteRead = Serial.read(); //read the byte
      if (byteRead != 13) //13 = CR
      {
        recBuffer[recByteCtr] = byteRead; //copy the byte read into the rec buffer
        recByteCtr++;
      }
      else //recbyte == 13
      {
        recBuffer[recByteCtr] = '\0'; //null terminate the rec'd string in rec buffer
        recByteCtr = 0;
        Serial.println(recBuffer);
        return 1;
      }
    }
  }
  return -1; //as we haven't received a complete message yet.
}
void parse()
{
  char* delim =",";
  char* Output1 = NULL;
  char* Output2 = NULL;
  char* Output3 = NULL;

  
  Output1=strtok(recBuffer,delim);
  Output2=strtok(NULL,delim);
  Output3=strtok(NULL,delim);
  
  Serial.print("Output 1:");
  Serial.println(Output1);
  Serial.print("Output 2:");
  Serial.println(Output2);
  Serial.print("Output 3:");
  Serial.println(Output3);

}
