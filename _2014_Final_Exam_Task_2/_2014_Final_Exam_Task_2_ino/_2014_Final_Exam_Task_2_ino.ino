/* 6915995 david panicker _2014 Final Exam Task 2
 /*
_2014 Final Exam Task 2.ino
This code waits for a * signifying the start of transmission
places the bytes that follow into a Global array called recBuffer[]
until a CR is received. Be sure to select carriage return from 
the pull-down menu on the comm windows so that a CR is sent when
you click on the send button.
*922,5.12,12.01,-11.58
*/
char RecBuffer [255]; //declare and initialize the RecBuffer array
int msgflag =0;
void setup()
{
   Serial.begin(9600);// opens serial port, sets data rate to 9600 bps
  
}
void loop()
{
  CheckForRecvChar();
   if (msgflag == 1)//if string is received sucessfully so start the the function parseRecBuffer
  { 
    parseRecBuffer();
    msgflag=0;//reset the flag
  }
  
}
void CheckForRecvChar()
{
  int16_t RecBufferCtr = 0;//declare and initialize the rec'd byte counter variable
  char ByteRecv;
  while (Serial.available()) //then let's read the byte
  {
    if (RecBufferCtr == 0)//then check to see if it's the start of a message
    {
      ByteRecv=Serial.read(); //read the byte
      if (ByteRecv == '*')
      { 
        RecBuffer[RecBufferCtr] = ByteRecv; //copy the byte read into the rec buffer
        RecBufferCtr++;
      }
    }
    if (RecBufferCtr != 0)//then we're reading bytes after receiving an STX in the message stream
    { 
      ByteRecv=Serial.read();//read the byte
      if (ByteRecv != 13)//13 = CR
      { 
        RecBuffer[RecBufferCtr] = ByteRecv;//copy the byte read into the rec buffer
        RecBufferCtr++;
      }
      else
      {
        RecBuffer[RecBufferCtr] = '\0';//null terminate the rec'd string in rec buffer
        RecBufferCtr= 0;
        msgflag = 1;//string is received sucessfully
      }
    }
  }
}
void parseRecBuffer()
{
  char* delim =",";//declare and initialize the pointer 
  char* Output0 = NULL;//declare and initialize the  pointer 
  char* Output1 = NULL;//declare and initialize the  pointer 
  char* Output2 = NULL;//declare and initialize the  pointer 
  char* Output3 = NULL;//declare and initialize the pointer 

  
  Output0=strtok(RecBuffer,delim);//breaking the string in 4 parts(part 1)
  Output1=strtok(NULL,delim);//part 2
  Output2=strtok(NULL,delim);//part 3
  Output3=strtok(NULL,delim);//part 4
  
  Serial.print("Output 1: ");//printing the output 1
  Serial.print(Output1);
  Serial.println(" V");
  Serial.print("Output 2: ");//printing the output 2
  Serial.print(Output2);
  Serial.println(" V");
  Serial.print("Output 3: ");//printing the output 3
  Serial.print(Output3);
  Serial.println(" V");
}









