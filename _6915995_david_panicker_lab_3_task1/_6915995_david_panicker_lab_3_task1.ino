/* 6915995 david panicker lab 3 task1*/

const int ledPin = 16;


// the setup() method runs once, when the sketch starts

void setup() 
{

// initialize the digital pin as an output.
  pinMode(ledPin, OUTPUT);


}
// the loop() methor runs over and over again,
// as long as the board has power
void loop() 
{


  blink_led1();  //blink_led fuction called once
  blink_led1();  //blink_led fuction called once again
  delay(1000);   // wait for a second



}

void blink_led1()
{
  digitalWrite(ledPin, HIGH);   // set the LED on
  delay(100);                  // wait for a 100 millisecond
  digitalWrite(ledPin, LOW);    // set the LED off
  delay(100);                  // wait for a 100 millisecond

}



