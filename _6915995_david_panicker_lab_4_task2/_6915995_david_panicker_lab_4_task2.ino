// _6915995_david_panicker_lab_4_task2
//pin 23 choosen as tone pin

const int tonePin = 23;

// the setup() method runs once, when the sketch starts

void setup() {
  pinMode(tonePin,OUTPUT);

}


void loop() {
  char hbday[] ="gAgCBgAgDCgGECBAFECDC";   
  playSong(hbday);
}

int8_t playNote(char note)
{
  switch (note)
  {
  case 'C'  : 
    tone (23,523,499); //plays the C for 499ms
    delay(500); 
  return 1;
         
  case 'D'  : 
    tone(23,587,499); //plays the D for 499ms
    delay(500);
  return 1;

  case 'E'  : 
    tone (23,659,499); //plays the E for 499ms
    delay(500);
  return 1;

  case 'F'  : 
    tone(23,699,492); //plays the F for 492ms
    delay(500);
  return 1;

  case 'G'  : 
    tone (23,794,499); //plays the G for 499ms
    delay(500);
  return 1;

  case 'A'  : 
    tone(23,440,499); //plays the A for 499ms
    delay(500);
  return 1;

  case 'B'  : 
    tone (23,494,498); //plays the B for 4980ms
    delay(499);
  return 1;

  case 'g'  : 
    tone (23,392,499);//plays the g for 499ms
    delay(500); 
    return 1;
  default:
  return -1;
  }
}

void playSong(char song[])
{

  for(int16_t i=0;song[i]!='\0';i++)
  {
    playNote(song[i]);
  }

}





