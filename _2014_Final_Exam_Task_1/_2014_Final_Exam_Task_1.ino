/* 2014 Final Exam Task 1 Code.ino
This code is the code to be used for the June 2014 Final Exam
for Prog8120.
*/

const int8_t spkrPin = 12;//pin 12 selested as the speaker pin

void setup()
{
  pinMode(spkrPin, OUTPUT);
  Serial.begin(9600);
}

void loop()
{
   beepSpkr(1000,3,100,100);//function called beep speaker
   delay(1000);// delay of 1 second
}

int8_t beepSpkr(int16_t frequency, int8_t numberOfTimes, int16_t onDuration, int16_t offDuration)
{
   for(int8_t x = 0; x<numberOfTimes; x++) //number of times the speaker beeps using for loop
  {
     tone(spkrPin, frequency, onDuration);
     delay(onDuration + offDuration);
  } 
  return 1;//returning the value 1 for the function beepSpkr
}
