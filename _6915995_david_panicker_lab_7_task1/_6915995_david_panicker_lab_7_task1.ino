/*_6915995_david_panicker_lab_7_task1
 serial rx2
 This code waits for a $ signifying the start of transmission
 places the bytes that follow into an array called recBuffer[]
 until a CR is received. Be sure to select carriage return from 
 the pull-down menu on the comm windows so that a CR is sent when
 you click on the send button.
 char GPSstring[] = "$GPGGA,014729.10,4303.5753,N,08019.0810,W,1,6,1.761,214.682,M,0,M,0,*5D";
  
 */

char RecBuffer [255];// gloabal value
int msgflag =0;
void setup()
{
   Serial.begin(9600);// opens serial port, sets data rate to 9600 bps
}
void loop()
{
  CheckForRecvChar();
  if (msgflag == 1)
  { 
    parse();
    msgflag=0;//reset the flag
  }
}
void CheckForRecvChar()
{
  int16_t RecBufferCtr = 0;//declare and initialize the rec'd byte counter variable
  char ByteRecv;
  while (Serial.available()) //then let's read the byte
  {
    if (RecBufferCtr == 0)//then check to see if it's the start of a message
    {
      ByteRecv=Serial.read();//read the byte
      if (ByteRecv == '$')
      { 
        RecBuffer[RecBufferCtr] = ByteRecv;//copy the byte read into the rec buffer
        RecBufferCtr++;
      }
    }
    if (RecBufferCtr != 0)//then we're reading bytes after receiving an STX in the message stream
    { 
      ByteRecv=Serial.read();//read the byte
      if (ByteRecv != 13)//13 = CR
      { 
        RecBuffer[RecBufferCtr] = ByteRecv;//copy the byte read into the rec buffer
        RecBufferCtr++;
      }
      else//recbyte == 13
      {
        RecBuffer[RecBufferCtr] = '\0';//null terminate the rec'd string in rec buffer
        RecBufferCtr= 0;
        Serial.println(RecBuffer);
        msgflag = 1;
      }
    }
  }
}
void parse()
{
  char* delim =",";//a comma is the delimiter
  char* SentenceID = NULL;//initialising pointer of char type
  char* UTC_time = NULL;
  char* Latitude = NULL;
  char* Latitudedir = NULL;
  char* Longitude = NULL;
  char* Longitudedir = NULL;
  char* PositionFix  = NULL;
  char* SatellitesUsed  = NULL;
  char* HDOP  = NULL;
  char* Altitude = NULL;
  char* Altitudemetres = NULL;
  char* Geoidseparation = NULL;
  char* Geoidseparationmetres = NULL;
  char* DGPSAge = NULL;
  char* Checksum = NULL;
  
  //Breaking the array and storing it in the vartiables as strings
  SentenceID=strtok(RecBuffer,delim);
  UTC_time=strtok(NULL,delim);
  Latitude=strtok(NULL,delim);
  Latitudedir=strtok(NULL,delim);
  Longitude=strtok(NULL,delim);
  Longitudedir=strtok(NULL,delim);
  PositionFix=strtok(NULL,delim);
  SatellitesUsed=strtok(NULL,delim);
  HDOP=strtok(NULL,delim);
  Altitude=strtok(NULL,delim);
  Altitudemetres=strtok(NULL,delim);
  Geoidseparation=strtok(NULL,delim);
  Geoidseparationmetres=strtok(NULL,delim);
  DGPSAge=strtok(NULL,delim);
  Checksum=strtok(NULL,delim);
  //printing the values stores by the variables
  Serial.print("SentenceID = ");
  Serial.println(SentenceID);
  Serial.print("UTC_time = ");
  Serial.println(UTC_time);
  Serial.print("Latitude = ");
  Serial.println(Latitude);
  Serial.print("Latitudedir = ");
  Serial.println(Latitudedir);
  Serial.print("Longitude = ");
  Serial.println(Longitude);
  Serial.print("Longitudedir = ");
  Serial.println(Longitudedir);
  Serial.print("PositionFix = ");
  Serial.println(PositionFix);
  Serial.print("SatellitesUsed = ");
  Serial.println(SatellitesUsed);
  Serial.print("HDOP = ");
  Serial.println(HDOP);
  Serial.print("Altitude = ");
  Serial.println(Altitude);
  Serial.print("Altitudemetres = ");
  Serial.println(Altitudemetres);
  Serial.print("Geoidseparation = ");
  Serial.println(Geoidseparation);
  Serial.print("Geoidseparationmetres = ");
  Serial.println(Geoidseparationmetres);
  Serial.print("DGPSAge = ");
  Serial.println(DGPSAge);
  Serial.print("Checksum = ");
  Serial.println(Checksum);
 
                
}
