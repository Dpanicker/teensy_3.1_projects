
// _6915995_david_panicker_lab_5_task6
//pin 24 choosen as Analog pin
const int Pot = 23;
char strBuffer[256];//buffer where the resulting C-string is stored
float trimpot_100 ;
const float x= 0.04887585532;//scaling factor
// the setup() method runs once, when the sketch starts
void setup() {
  pinMode(Pot,INPUT);
  Serial.begin(9600);

}
void loop() {
  int16_t val;
  val=analogRead(Pot);
  trimpot_100=x*(float)val;//typecast
  sprintf(strBuffer,"Value read is : %d.%d",((int)trimpot_100/10),((int)trimpot_100%10));
  Serial.println(strBuffer);
  delay(500);
}





