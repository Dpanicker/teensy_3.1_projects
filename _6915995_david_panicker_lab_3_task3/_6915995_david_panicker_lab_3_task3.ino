/* 6915995 david panicker lab 3 task3*/

const int ledPin = 16 ;
const int pullup = 23 ;


//prototypes


void setup() 
{
  pinMode(ledPin, OUTPUT);
  Serial.begin(9600); 
  pinMode(pullup, INPUT_PULLUP);
}

void loop() 
{
  if(digitalRead(23)==LOW)
  {
    flash_Led(500,1000);
  }
  else
  {
    flashSOS();

  }


}  

int8_t  flash_Led(int16_t ledon,int16_t ledoff)
{
  digitalWrite(ledPin, HIGH);   // set the LED on
  delay(ledon);                  // wait for input as per ledon
  digitalWrite(ledPin, LOW);    // set the LED off
  delay(ledoff);                // wait for input as per ledoff

}

int8_t flashSOS()
{


  flashDot();
  flashDot();
  flashDot();      // for the letter "S"
  flashDash(); 
  flashDash();
  flashDash();   // for the letter "O"
  flashDot();
  flashDot();
  flashDot();      // for the letter "S"
  delay(3000); 

}

void flashDot()
{
  flash_Led(200,200); 
}

void flashDash()
{
  flash_Led(500,200);
}









